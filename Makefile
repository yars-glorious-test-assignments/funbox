ut:
	REDIS_PORT=6379 REDIS_HOST=localhost mix test --trace --max-failures 1 --slowest 5
it:
	REDIS_PORT=6379 REDIS_HOST=localhost MIX_ENV=integration mix test --trace --max-failures 1 --slowest 5 --cover
ts:
	make ut
	make it
lint:
	mix credo
	mix credo -C tests
run: 
	REDIS_PORT=6379 REDIS_HOST=localhost mix run --no-halt