defmodule FTA.MixProject do
  use Mix.Project

  def project do
    [
      app: :fta,
      version: "0.1.0",
      elixir: "~> 1.9",
      elixirc_paths: elixirc_paths(Mix.env()),
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      test_paths: test_paths(Mix.env()),
      elixirc_options: [warnings_as_errors: true],
      dialyzer: [
        plt_file: {:no_warn, "priv/plts/dialyzer.plt"},
        flags: ["-Wunmatched_returns", :error_handling, :race_conditions, :underspecs, :no_opaque]
      ],
    ]
  end

  defp elixirc_paths(:test), do: [
    "lib",
    "test/support",
    "test/unit/support"
  ]
  defp elixirc_paths(:integration), do: [
    "lib",
    "test/support",
    "test/integration/support",
    "test/integration/fta/web/support",
  ]

  defp elixirc_paths(_), do: ["lib"]

  defp test_paths(:integration), do: ["test/integration"]
  defp test_paths(_),            do: ["test/unit"]

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      mod: {FTA.Application, []},
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:redix, ">= 0.0.0"},
      {:castore, ">= 0.0.0"},
      {:credo, "~> 1.5", only: [:dev, :test], runtime: false},
      {:dialyxir, "~> 1.0", only: [:dev], runtime: false},
      {:stream_data, "~> 0.5", only: [:test, :integration]},
      {:assert_eventually, "~> 0.2.0", only: [:test, :integration]},
      {:httpoison, "~> 1.8"},
      {:jason, "~> 1.2"},
      {:plug_cowboy, "~> 2.0"},
      {:ex_doc, "~> 0.24", only: :dev, runtime: false},
    ]
  end
end
