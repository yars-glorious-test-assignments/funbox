use Mix.Config

config :fta,
  redis_adapter: Redix,
  redis_host: System.get_env("REDIS_HOST"),
  redis_port: System.get_env("REDIS_PORT"),
  redis_process_name: :redis_connection

if Mix.env() in [:test, :integration], do: import_config "test_config.exs"
