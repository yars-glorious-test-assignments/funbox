use Mix.Config

config :stream_data,
  # I wish CI runs number was bigger, but free CI runners are slow
  max_runs: if(System.get_env("CI"), do: 40, else: 20)
