defmodule FTA.Application do
  @moduledoc false

  use Application

  alias FTA.Web.RootPlug

  @impl Application
  def start(_type, _args) do
    redis_process_name = Application.get_env(:fta, :redis_process_name)
    redis_host = Application.get_env(:fta, :redis_host)
    redis_port = Application.get_env(:fta, :redis_port)
    int_redis_port = String.to_integer(redis_port)

    children = [
      {Redix, host: redis_host, port: int_redis_port, name: redis_process_name},
      {Plug.Cowboy, scheme: :http, plug: RootPlug, options: [port: 4001]}
    ]

    opts = [strategy: :one_for_one, name: TFA.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
