defmodule FTA.App do
  @moduledoc """
    The module contains API-level functions to register and retrieve data from FTA storage.
  """
  alias FTA.App.DomainNameExtractor
  alias FTA.App.Storage

  defp get_redis_process_name do
    Application.get_env(:fta, :redis_process_name)
  end

  defp redis_conn do
    get_redis_process_name()
  end

  @doc """
    A function that takes an integer identifier and a list of links, attempts to parse domain names out of each link
    and persists it to storage with {:ok, list} response where list is a collection of persisted domain names.
    If there is at least one link from which domain name cannot be parsed, none of the domain names in the batch
    is persisted and {:error, list} is returned where list contains all the links which function failed to extract
    domain name from.
  """

  @spec process_links(integer(), list(binary())) ::
          {:ok, [binary()]}
          | {:error, [binary()]}
          | {:error, :key_is_nil}
          | {:error, :links_is_nil}
          | {:error, :links_list_is_empty}
          | {:error, :db_error}
  def process_links(nil, _links), do: {:error, :key_is_nil}
  def process_links(_key, nil), do: {:error, :links_is_nil}
  def process_links(_key, []), do: {:error, :links_list_is_empty}

  def process_links(key, links) do
    {domain_names, broken_links} = parse_links(links)
    case Enum.count(broken_links) > 0 do
      true  -> {:error, broken_links}
      false -> do_persist(redis_conn(), key, domain_names)
    end
  end

  defp parse_links(links) do
    Enum.reduce(links, {[], []}, fn link, acc ->
      {domain_names, broken_links} = acc
      case DomainNameExtractor.parse(link) do
        {:error}           -> {domain_names, [link | broken_links]}
        {:ok, domain_name} -> {[domain_name | domain_names], broken_links}
      end
    end)
  end

  defp do_persist(conn, key, list) do
    case Storage.persist(conn, key, list) do
      {:ok, _count}  -> {:ok, list}
      {:error, _any} -> {:error, :db_error}
    end
  end

  @doc """
    A function returns {:ok, list} where list contains registered domains predicated by two given identifiers
     or {:error, reason} should error occur.
  """
  @spec get_registered_domain_names(integer, integer) ::
        {:ok, list(binary())}
          |{:error, :range_error}
          | {:error, :db_error}
  def get_registered_domain_names(from, to) do
    case  Storage.retrieve_uniq_in_range(redis_conn(), from, to) do
      {:ok, list}            -> {:ok, list}
      {:error, :range_error} -> {:error, :range_error}
      _rest                  -> {:error, :db_error}
    end
  end
end
