defmodule FTA.Web.VisitedLinksPlug do
  @moduledoc """
    A module containing plug function `run(conn, options)` and supporting logic to handle requests which register new visited links.
  """

  import FTA.Web.Helpers

  defp get_app() do
    FTA.App
  end

  @spec run(Plug.Conn.t(), any) :: Plug.Conn.t()
  def run(conn, _options) do
    time_of_req = get_request_time()

    conn
    |> extract_params()
    |> parse_params()
    |> process_request(time_of_req, conn)
  end

  defp get_request_time() do
    :erlang.system_time(:second)
  end

  defp extract_params(conn), do: conn.params

  defp parse_params(%{"links" => links} = _body_params) when is_list(links) do
    case payload_looks_valid?(links) do
      true   -> {:ok, links}
      _false -> {:error, :bad_payload}
    end
  end

  defp parse_params(_body_params) do
    {:error, :bad_payload}
  end

  defp payload_looks_valid?(links) do
    Enum.all?(links, &(is_binary(&1)))
  end

  defp process_request({:ok, links}, time, conn) do
    case get_app().process_links(time, links) do
      {:error, list} when is_list(list) -> respond_with_error(conn, 400)
      {:error, :links_list_is_empty} -> respond_with_error(conn, 400)
      {:error, _} -> respond_with_error(conn, 500)
      {:ok, _msg}  -> respond_with_ok(conn)
    end
  end

  defp process_request(_error, _time, conn) do
    respond_with_error(conn, 400)
  end
end
