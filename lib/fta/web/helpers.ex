defmodule FTA.Web.Helpers do
  @moduledoc """
    A module containing helpers for plug-like components.
  """
  import Plug.Conn
  @spec error_body :: binary
  def error_body() do
    Jason.encode!(%{status: :error})
  end

  @spec ok_body :: binary
  def ok_body() do
    Jason.encode!(%{status: :ok})
  end

  @spec ok_body(map()) :: binary
  def ok_body(body) do
    body
    |> Map.merge(%{status: :ok})
    |> Jason.encode!()
  end

  @spec respond_with_ok(Plug.Conn.t()) :: Plug.Conn.t()
  def respond_with_ok(conn), do: respond_with_ok(conn, ok_body())

  @spec respond_with_ok(Plug.Conn.t(), binary()) :: Plug.Conn.t()
  def respond_with_ok(conn, body) do
    conn
    |> Plug.Conn.put_resp_header("content-type", "application/json")
    |> send_resp(200, body)
  end

  @spec respond_with_error(Plug.Conn.t(), integer()) :: Plug.Conn.t()
  def respond_with_error(conn, code) do
    conn
    |> Plug.Conn.put_resp_header("content-type", "application/json")
    |> send_resp(code, error_body())
  end
end
