defmodule FTA.Web.RootPlug do
  @moduledoc """
    An entry point pipeline for HTTP requests.
  """

  use Plug.Router
  use Plug.ErrorHandler

  import FTA.Web.Helpers

  alias FTA.Web.VisitedDomainsPlug
  alias FTA.Web.VisitedLinksPlug

  plug :match
  plug(Plug.Parsers, parsers: [:json], json_decoder: Jason)
  plug :dispatch

  options "/visited_links" do
    conn
    |> Plug.Conn.put_resp_header("Allow", "OPTIONS, POST")
    |> send_resp(204, "")
  end

  post "/visited_links" do
    VisitedLinksPlug.run(conn, [])
  end

  options "/visited_domains" do
    conn
    |> Plug.Conn.put_resp_header("Allow", "OPTIONS, GET")
    |> send_resp(204, "")
  end

  get "/visited_domains" do
    VisitedDomainsPlug.run(conn, [])
  end

  match _ do
    respond_with_error(conn, 404)
  end

  @spec handle_errors(Plug.Conn.t(), %{
    type: :error | :throw | :exit,
    reason: Exception.t() | term(),
    stacktrace: Exception.stacktrace()
  }) :: Plug.Conn.t()
  def handle_errors(conn, %{kind: :error, reason: %Plug.Parsers.ParseError{}}) do
    respond_with_error(conn, 400)
  end

  def handle_errors(conn, _other) do
    respond_with_error(conn, 500)
  end
end
