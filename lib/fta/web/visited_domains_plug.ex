defmodule FTA.Web.VisitedDomainsPlug do
  @moduledoc """
    A module containing plug function `run(conn, options)` and supporting logic to handle requests to show registered domains.
  """

  import FTA.Web.Helpers

  defp app() do
    FTA.App
  end

  @spec run(Plug.Conn.t(), any) :: Plug.Conn.t()
  def run(conn, _options) do
    conn
    |> extract_params()
    |> validate_params()
    |> parse_params()
    |> process_request(conn)
  end

  defp extract_params(conn), do: conn.params

  defp validate_params(%{"from" => from, "to" => to}) do
    %{"from" => from, "to" => to}
  end
  defp validate_params(_rest), do:
    {:error, :range_params_missing}

  defp parse_params(%{"from" => from, "to" => to}) do
    with {int_from, ""} <- Integer.parse(from),
         {int_to, ""}   <- Integer.parse(to)
    do
      %{"from" => int_from, "to" => int_to}
    else
      _failed_parsing -> {:error, :params_not_integer}
    end
  end
  defp parse_params({:error, _reason} = error), do: error

  defp process_request(%{"from" => from, "to" => to}, conn) do
    case app().get_registered_domain_names(from, to) do
      {:ok, list} -> respond_with_ok(conn, ok_body(%{domains: list}))
      {:error, :range_error} -> respond_with_error(conn, 400)
      {:error, :db_error}    -> respond_with_error(conn, 500)
    end
  end
  defp process_request({:error, _reason}, conn), do: respond_with_error(conn, 400)
end
