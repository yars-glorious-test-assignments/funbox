defmodule FTA.App.Storage do
  @moduledoc """
    A module that contains persistence logic
  """

  @type redis_connection_error :: %{reason: atom}
  @type redis_error            :: %{message: binary}

  @storage_index_key "storage_index"

  @spec redis() :: module()
  defp redis() do
    Application.get_env(:fta, :redis_adapter)
  end

  @spec persist(GenServer.server(), integer(), list(binary())) :: (
    {:ok, integer()} | {:error, atom()} | redis_error() | redis_connection_error()
  )
  def persist(_conn, nil, _values), do: {:error, :key_is_nil}
  def persist(_conn, _key, nil), do: {:error, :value_is_nil}
  def persist(_conn, _key, []), do: {:ok, 0}
  def persist(conn, key, values) do
    case persist_pipeline(conn, key, values) do
      {:ok, [head | _rest]} -> {:ok, head}
      error                 -> error
    end
  end

  defp persist_pipeline(conn, key, values) do
    redis().pipeline(conn, [
      ["SADD", key] ++ values,
      ["ZADD", @storage_index_key, key, key]
    ])
  end

  @spec retrieve_uniq_in_range(GenServer.server(), integer(), integer()) :: (
    {:ok, list(binary())} | {:error, :range_error} | redis_error() | redis_connection_error()
  )
  def retrieve_uniq_in_range(_conn, from, to) when from > to, do: {:error, :range_error}
  def retrieve_uniq_in_range(_conn, from, to) when is_nil(from) or is_nil(to), do: {:error, :range_error}
  def retrieve_uniq_in_range(conn, from, to) do
    case redis().command(conn, ["ZRANGEBYSCORE", @storage_index_key, from, to]) do
      {:ok, index_list} -> fetch_uniq_data_from_sets(conn, index_list)
      err               -> err
    end
  end

  defp fetch_uniq_data_from_sets(_conn, []), do: {:ok, []}
  defp fetch_uniq_data_from_sets(conn, set_list) do
    redis().command(conn, ["SUNION"] ++ set_list)
  end
end
