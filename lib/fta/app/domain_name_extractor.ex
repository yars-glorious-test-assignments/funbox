defmodule FTA.App.DomainNameExtractor do
  @moduledoc """
    The DomainNameExtractor module contains a `parse/1` function which takes an arbitrary link
    and tries to extract a valid domain name. The function returns either {:ok, domain_name} or {:error} if
    it fails to extract the domain name out of provided binary.
  """

  @spec parse(binary()) :: {:error} | {:ok, binary()}
  def parse(text) when is_binary(text) do
    text
    |> URI.decode()
    |> normalize_url()
    |> URI.parse()
    |> Map.get(:host)
    |> filter_out_odd_stuff()
    |> wrap_response()
  end
  def parse(_any), do: {:error}

  defp normalize_url(text) do
    scheme = text |> URI.parse() |> Map.get(:scheme)
    prepend_default_scheme(text, is_nil(scheme))
  end

  defp prepend_default_scheme(text, true) do
    "//#{text}"
  end
  defp prepend_default_scheme(text, _scheme_missing?) do
    text
  end

  defp filter_out_odd_stuff(domain_name_candidate) do
    domain_name_candidate
    |> filter_out_ipv6()
    |> filter_out_ipv4()
    |> filter_out_odd_hostnames()
  end

  defp filter_out_ipv6(nil), do: nil
  defp filter_out_ipv6(string) do
    case String.match?(string, ~r/[:]/) do
      true -> nil
      _f   -> string
    end
  end

  defp filter_out_ipv4(nil), do: nil
  defp filter_out_ipv4(string) do
    case String.match?(string, ~r/^([0-9]+\.)+[0-9]+$/) do
      true -> nil
      _f   -> string
    end
  end

  defp filter_out_odd_hostnames(text) do
    text
    |> filter_out_empty_subdomain_part()
    |> filter_out_hosts_containg_odd_symbols()
  end

  defp filter_out_empty_subdomain_part(nil), do: nil
  defp filter_out_empty_subdomain_part(text) do
    case String.match?(text, ~r/[.]{2,}/) do
      true -> nil
      _f   -> text
    end
  end

  defp filter_out_hosts_containg_odd_symbols(nil), do: nil
  defp filter_out_hosts_containg_odd_symbols(text) do
    # disqualifing domain names which contain odd symbols
    case String.match?(text, ~r/[|&\ \n]/) do
      true  -> nil
      _rest -> text
    end
  end

  defp wrap_response(nil), do: {:error}
  defp wrap_response(""), do: {:error}
  defp wrap_response(domain_name) do
    {:ok, String.downcase(domain_name)}
  end
end
