FROM elixir:1.12-alpine

WORKDIR /app

COPY mix.exs .
COPY mix.lock .
COPY lib lib
COPY config config

ENV MIX_ENV=prod

RUN set -ex \
    && mix local.hex --force \
    && mix local.rebar --force \
    && mix deps.get \
    && mix deps.compile \
    && mix compile

CMD ["mix", "run", "--no-halt"]