defmodule DomainNameExtractorTest do
  use ExUnitProperties
  use ExUnit.Case, async: true

  import URIDataGenerators

  @subject FTA.App.DomainNameExtractor

  describe "parse(binary())" do
    test "returns {:error} when presented with nil" do
      assert {:error} = @subject.parse(nil)
    end

    test "returns {:error} when presented with empty string" do
      assert {:error} = @subject.parse("")
    end

    test "returns {:error} when presented with bunch of spaces" do
      assert {:error} = @subject.parse("  ")
    end

    test "returns {:error} when presented with multiline string" do
      assert {:error} =
               @subject.parse("""
               localhost

               """)
    end

    test "parses out domain name out of a string #1" do
      {:ok, "local"} = @subject.parse("local")
    end

    test "parses out domain name out of a string ending with /" do
      {:ok, "local"} = @subject.parse("local/")
    end

    test "parses out domain name out of a string #2" do
      {:ok, "funbox.ru"} = @subject.parse("funbox.ru")
    end

    test "parses out domain name out of complex URI" do
      {:ok, "test.com"} = @subject.parse("https://test.com/path/?q='123'&page=20")
    end

    test "fragment in url part won't brake parsing" do
      {:ok, "funbox.ru"} = @subject.parse("funbox.ru/#main")
    end

    test "rejects IPv6 address as host" do
      {:error} = @subject.parse("[2001:0db8:85a3:0000:0000:8a2e:0370:7334]/somepath")
    end

    test "filters out IPv4 address as host" do
      {:error} = @subject.parse("https://10.20.32.16/path/?q='123'&page=20")
    end

    test "would filter out wrong formated IPv4 addresses as well" do
      {:error} = @subject.parse("https://10.20.32.16.29/path/?q='123'&page=20")
    end

    test "works with domain name that looks like IPv4 but no quite yet" do
      {:ok, "10.20.30.50.test.com"} =
        @subject.parse("https://10.20.30.50.test.com/path/?q='123'&page=20")
    end

    test "rejects domain with two or more dots in a row like test..com" do
      ["test..com", "test.........com"]
      |> Enum.each(fn text ->
        assert {:error} = @subject.parse(text)
      end)
    end

    test "rejects domain containing '|' symbol as part of it" do
      ["http://localhost|", "http://|", "|", "http://local|host", "http://|localhost"]
      |> Enum.each(fn text ->
        assert {:error} = @subject.parse(text)
      end)
    end

    test "allows HTTP AUTH as part of a link" do
      {:ok, "test.com"} = @subject.parse("user@test.com")
    end

    test "rejects domain containing & as a part of it" do
      ["http://&localhost", "&localhost", "local&host", "localhost&"]
      |> Enum.each(fn text ->
        assert {:error} = @subject.parse(text)
      end)
    end

    test "works with national domains" do
      {:ok, "анекдот.рф"} = @subject.parse("анекдот.рф")
    end

    test "works with encoded national domain" do
      {:ok, "анекдот.рф"} =
        @subject.parse("%D0%B0%D0%BD%D0%B5%D0%BA%D0%B4%D0%BE%D1%82.%D1%80%D1%84")
    end

    test "decodes URI" do
      assert {:ok, "elixir-lang.org"} = @subject.parse("https%3A%2F%2Felixir-lang.org")
    end

    test "responds with error when incorrect URIs are provided" do
      ["", "//?a=b", "http://", "http:///a", "http:///a/"]
      |> Enum.each(fn text ->
        assert {:error} = @subject.parse(text)
      end)
    end

    test "responds with an error when its just a scheme" do
      assert {:error} = @subject.parse("https://")
    end

    test "domain name is lowercased when returned by parser" do
      assert {:ok, "ya.ru"} = @subject.parse("YA.RU")
    end
  end

  describe "extraction properties" do
    property "rejects any host from bad domain list" do
      check all(link <- bad_link_gen()) do
        assert {:error} = @subject.parse(link)
      end
    end

    @tag :slow
    @tag timeout: 240_000
    property "for every (scheme://)(domain_name)(/rest) test responds with {:ok, domain_name}" do
      check all(
              scheme <- scheme_gen(),
              domain_piece <- domain_piece_gen(),
              domain_name = domain_name_fmt(domain_piece),
              path_list <- path_list_gen(),
              path = path_fmt(path_list),
              path_ending <- path_ending_gen(),
              query_pairs <- query_pairs_gen(),
              query = query_string_fmt(query_pairs)
            ) do
        downcased_domain_name = String.downcase(domain_name)
        link = "#{scheme}#{domain_name}#{path}#{path_ending}#{query}"
        assert {:ok, ^downcased_domain_name} = @subject.parse(link)
      end
    end
  end
end
