defmodule AppTest do
  use ExUnitProperties
  use ExUnit.Case, async: false

  import RedisHelpers
  import CustomMatchers
  import URIDataGenerators
  import MiscGenerators

  @subject FTA.App
  doctest @subject

  @this_app :fta
  @storage_index_key "storage_index"
  @redis_process_name_key :redis_process_name

  setup_all do
    redis_port = Application.get_env(:fta, :redis_port) |> String.to_integer()
    redis_host = Application.get_env(:fta, :redis_host)
    {:ok, conn} = Redix.start_link(host: redis_host, port: redis_port)
    [redis_conn: conn]
  end

  setup %{redis_conn: redis_conn} do
    flush_storage(redis_conn)
    []
  end

  describe ".process_links(key, links)" do
    test "retuns {:error, :db_error} when link to redis is broken" do
      invalid_conn_name = :invalid_conn
      {:ok, _pid} = Redix.start_link(host: "invalidhost", port: 1234, name: invalid_conn_name)
      old_val = Application.get_env(@this_app, @redis_process_name_key)
      Application.put_env(@this_app, @redis_process_name_key, invalid_conn_name)
      [key] = Enum.take(storage_key_gen(), 1)
      [links] = Enum.take(list_of_links_gen(), 1)

      assert {:error, :db_error} = @subject.process_links(key, links)

      Application.put_env(@this_app, @redis_process_name_key, old_val)
    end

    test "returns {:error, :key_is_nil} when key is nil" do
      [links] = Enum.take(list_of_links_gen(), 1)

      assert {:error, :key_is_nil} = @subject.process_links(nil, links)
    end

    property "returns {:error, :links_is_nil} when links is nil" do
      check all (key <- storage_key_gen()) do
        assert {:error, :links_is_nil} = @subject.process_links(key, nil)
      end
    end

    property "returns {:error, :list_is_empty} when links list is empty" do
      check all (key <- storage_key_gen()) do
        assert {:error, :links_list_is_empty} = @subject.process_links(key, [])
      end
    end

    property "returns {:error, rejected_links_list} then there is at least 1 incorrect link supplied" do
      check all(
        key <- storage_key_gen(),
        links <- list_of_links_gen(),
        a_bad_link <- bad_link_gen()
      ) do
        shuffled_links = Enum.shuffle([a_bad_link | links])

        assert {:error, [^a_bad_link]} = @subject.process_links(key, shuffled_links)
      end
    end

    property "filters all bad links supplied", %{
      redis_conn: conn
    } do
      check all(
        key <- storage_key_gen(),
        links <- list_of_links_gen(),
        bad_links <- list_of_bad_links_gen()
      ) do
        shuffled_links = Enum.shuffle(links ++ bad_links)
        {:error, filtered_links} = @subject.process_links(key, shuffled_links)
        flush_storage(conn)

        assert [] = filtered_links -- bad_links
        assert [] = bad_links -- filtered_links
      end
    end

    property "nothing is persisted when there is at least one incorrect link", %{
      redis_conn: conn
    } do
      check all(
        key <- storage_key_gen(),
        links <- list_of_links_gen(),
        a_bad_link <- bad_link_gen()
      ) do
        shuffled_links = Enum.shuffle([a_bad_link | links])
        @subject.process_links(key, shuffled_links)

        assert {:ok, []} = get_set_members(conn, key)
      end
    end

    property "returns {:ok, list_of_domain_names} when provided key and list of links are valid", %{
      redis_conn: conn
    } do
      check all(
        key <- storage_key_gen(),
        list_of_links_and_domain_names <- list_of_links_and_domain_names_gen(),
        {links, domain_names} = {
          Enum.map(list_of_links_and_domain_names, fn {l, _dn} -> l end),
          Enum.map(list_of_links_and_domain_names, fn {_l, dn} -> dn end)
        }
      ) do
        assert {:ok, returned_domain_names} = @subject.process_links(key, links)
        flush_storage(conn)

        assert [] = returned_domain_names -- domain_names
        assert [] = domain_names -- returned_domain_names
      end
    end

    property "every domain name is persisted when provided key and list of links are valid", %{
      redis_conn: conn
    } do
      check all(
        key <- storage_key_gen(),
        list_of_links_and_domain_names <- list_of_links_and_domain_names_gen(),
        {links, domain_names} = {
          Enum.map(list_of_links_and_domain_names, fn {l, _dn} -> l end),
          Enum.map(list_of_links_and_domain_names, fn {_l, dn} -> dn end)
        }
      ) do
        {:ok, _returned_domain_names} = @subject.process_links(key, links)

        for domain_name <- domain_names do
          assert domain_name |> stored_in_redis_set?(at_key: key, at: conn)
        end
      end
    end

    property "indexes all valid links locations with key and (score == key)", %{
      redis_conn: conn
    } do
      check all(
        key <- storage_key_gen(),
        links <- list_of_links_gen()
      ) do
        {:ok, _domain_names} = @subject.process_links(key, links)

        assert key |> stored_in_redis_sorted_set?(with_score: key, at_key: @storage_index_key, at: conn)
      end
    end
  end

  describe "get_registered_domain_names(from, to)" do
    property "returns {:error, connection_error} when conn is invalid" do
      invalid_conn_name = :invalid_conn
      {:ok, _pid} = Redix.start_link(host: "invalidhost", port: 1234, name: invalid_conn_name)
      old_val = Application.get_env(@this_app, @redis_process_name_key)
      Application.put_env(@this_app, @redis_process_name_key, invalid_conn_name)

      check all {from, to} <- non_desc_int_range_gen() do
        {:error, :db_error} = @subject.get_registered_domain_names(from, to)
      end

      Application.put_env(@this_app, @redis_process_name_key, old_val)
    end

    property "returns {:error, :range_error} when `to` is nil" do
      check all {from, _to} <- non_desc_int_range_gen() do
        {:error, :range_error} = @subject.get_registered_domain_names(from, nil)
      end
    end

    property "returns {:error, :range_error} when `from` is nil" do
      check all {_from, to} <- non_desc_int_range_gen() do
        {:error, :range_error} = @subject.get_registered_domain_names(nil, to)
      end
    end

    property "returns {:error, :range_error} when from > to" do
      check all {to, from} <- non_desc_int_range_gen(),
                to != from
      do
        {:error, :range_error} = @subject.get_registered_domain_names(from, to)
      end
    end

    property "returns {:ok, []} when no data stored for given range" do
      check all {from, to} <- non_desc_int_range_gen() do
        {:ok, []} = @subject.get_registered_domain_names(from, to)
      end
    end

    test "returns {:ok, list} containing stored data when there are stored data within given range", %{
      redis_conn: conn
    } do
      check all list_of_data    <- StreamData.list_of({storage_key_gen(), list_of_binary_gen()}, min_length: 1),
                stored_binaries =  Enum.flat_map(list_of_data, fn {_key, vals} -> vals end),
                keys_list       =  Enum.map(list_of_data, fn {key, _v} -> key end),
                min_key         =  Enum.min(keys_list),
                max_key         =  Enum.max(keys_list) do
        for {key, list} <- list_of_data do
          add_to_set(conn, key, list)
          add_to_sorted_set(conn, @storage_index_key, key, key)
        end

        {:ok, retrieved_list} = @subject.get_registered_domain_names(min_key, max_key)
        flush_storage(conn)
        assert Enum.all?(stored_binaries, fn stored_binary -> stored_binary in retrieved_list end)
      end
    end

    property "returned list never contains duplicate values", %{
      redis_conn: conn
    } do
      check all list_of_data    <- StreamData.list_of({storage_key_gen(), list_of_binary_gen()}, min_length: 1),
                keys_list       =  Enum.map(list_of_data, fn {key, _v} -> key end),
                max_key         =  Enum.max(keys_list),
                min_key         =  Enum.min(keys_list) do
        for {key, list} <- list_of_data do
          add_to_set(conn, key, list)
          add_to_sorted_set(conn, @storage_index_key, key, key)
        end
        {:ok, retrieved_list} = @subject.get_registered_domain_names(min_key, max_key)
        flush_storage(conn)

        uniqualized_retrieved_list = MapSet.new(retrieved_list) |> MapSet.to_list()
        assert Enum.count(retrieved_list) == Enum.count(uniqualized_retrieved_list)
      end
    end
  end
end
