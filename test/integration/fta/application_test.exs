defmodule ApplicationTest do
  use ExUnit.Case, async: false

  use AssertEventually, timeout: 1000, interval: 5

  @redis_process_name :redis_connection

  describe "Redix server process" do
    test "it is running when app starts" do
      redis_writer_pid = Process.whereis(@redis_process_name)
      assert Process.alive?(redis_writer_pid)
    end

    test "the process is restarted should it die" do
      redis_writer_pid = Process.whereis(@redis_process_name)

      Process.exit(redis_writer_pid, :kill)
      refute Process.alive?(redis_writer_pid)

      assert_eventually(
        Process.whereis(@redis_process_name)
        |> Process.alive?()
      )
    end
  end
end
