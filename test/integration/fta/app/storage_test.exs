defmodule StorageTest do
  use ExUnitProperties
  use ExUnit.Case, async: false

  import RedisHelpers
  import CustomMatchers
  import MiscGenerators

  @subject FTA.App.Storage
  doctest @subject

  @storage_index_key "storage_index"

  setup_all do
    redis_port = Application.get_env(:fta, :redis_port) |> String.to_integer()
    redis_host = Application.get_env(:fta, :redis_host)
    {:ok, conn} = Redix.start_link(host: redis_host, port: redis_port)
    {:ok, invalid_conn} = Redix.start_link(host: "invalidconn", port: 15_999)
    [redis_conn: conn, invalid_conn: invalid_conn]
  end

  setup %{redis_conn: redis_conn} do
    flush_storage(redis_conn)
    []
  end

  describe ".persist(conn, key, values)" do
    test "returns {:error, reason} when conn is invalid", %{invalid_conn: invalid_conn} do
      assert {:error, _reason} = @subject.persist(invalid_conn, 1, "binary")
    end

    test "returns {:error, :key_is_nil} when key is nil", %{redis_conn: conn} do
      assert {:error, :key_is_nil} = @subject.persist(conn, nil, "binary")
    end

    test "returns {:error, :value_is_nil} when value is nil", %{redis_conn: conn} do
      assert {:error, :value_is_nil} = @subject.persist(conn, 1, nil)
    end

    test "returns {:ok, 0} when empty list of values is provided", %{redis_conn: conn} do
      assert {:ok, 0} = @subject.persist(conn, 2, [])
    end

    test "returns {:ok, uniq_count} when arguments are valid", %{redis_conn: conn} do
      values = ["binary", "another", "binary", "another", "one"]
      assert {:ok, 3} = @subject.persist(conn, 1, values)
    end

    test "persists 'values' in redis set at 'key' when arguments are valid", %{redis_conn: conn} do
      values = ["binary", "another", "binary", "another", "one"]

      @subject.persist(conn, 3, values)

      for value <- values do
        assert value |> stored_in_redis_set?(at_key: 3, at: conn)
      end
    end

    property "any integer and non-empty list(binary) is persisted without an issue", %{redis_conn: conn} do
      check all key <- storage_key_gen(), list <- list_of_binary_gen() do
        assert {:ok, _} = @subject.persist(conn, key, list)
      end
    end

    test "does not update storage_index if 0 new items added", %{redis_conn: conn} do
      @subject.persist(conn, 1, [])
      assert {:ok, []} = get_sorted_set_members(conn, @storage_index_key)
    end

    property "for every key entry added there is a storage_index entry", %{redis_conn: conn} do
      check all key  <- storage_key_gen(),
                data <- list_of_binary_gen() do
          {:ok, _cnt} = @subject.persist(conn, key, data)
          assert "#{key}" |> stored_in_redis_sorted_set?(at_key: @storage_index_key, at: conn)
      end
    end

    @tag :slow
    property "for every index entry added there is a non-empty set", %{redis_conn: conn} do
      check all key  <- storage_key_gen(),
                data <- list_of_binary_gen() do
        {:ok, _cnt} = @subject.persist(conn, key, data)
      end
      {:ok, index_list} = get_sorted_set_members(conn, @storage_index_key)

      for entry <- index_list do
        set_key = String.to_integer(entry)
        {:ok, set_content} = get_set_members(conn, set_key)
        assert Enum.count(set_content) > 0
      end
    end
  end

  describe "retrieve_uniq_in_range(conn, from, to)" do
    property "returns {:error, reason} when conn is invalid", %{invalid_conn: invalid_conn} do
      check all {from, to} <- non_desc_int_range_gen() do
        {:error, _reason} = @subject.retrieve_uniq_in_range(invalid_conn, from, to)
      end
    end

    property "returns {:error, :range_error} when `to` is nil", %{redis_conn: conn} do
      check all {from, _to} <- non_desc_int_range_gen() do
        {:error, :range_error} = @subject.retrieve_uniq_in_range(conn, from, nil)
      end
    end

    property "returns {:error, :range_error} when `from` is nil", %{redis_conn: conn} do
      check all {_from, to} <- non_desc_int_range_gen() do
        {:error, :range_error} = @subject.retrieve_uniq_in_range(conn, nil, to)
      end
    end

    property "returns {:error, :range_error} when from > to", %{redis_conn: conn} do
      check all {to, from} <- non_desc_int_range_gen(),
                to != from
      do
        {:error, :range_error} = @subject.retrieve_uniq_in_range(conn, from, to)
      end
    end

    property "returns {:ok, []} when no data stored for given range", %{redis_conn: conn} do
      check all {from, to} <- non_desc_int_range_gen() do
        {:ok, []} = @subject.retrieve_uniq_in_range(conn, from, to)
      end
    end

    @tag :slow
    test "returns {:ok, list} containing stored data when there are stored data within given range", %{redis_conn: conn} do
      check all list_of_data    <- StreamData.list_of({storage_key_gen(), list_of_binary_gen()}, min_length: 1),
                stored_binaries =  Enum.flat_map(list_of_data, fn {_key, vals} -> vals end),
                keys_list       =  Enum.map(list_of_data, fn {key, _v} -> key end),
                min_key         =  Enum.min(keys_list),
                max_key         =  Enum.max(keys_list) do
        for {key, list} <- list_of_data do
          add_to_set(conn, key, list)
          add_to_sorted_set(conn, @storage_index_key, key, key)
        end

        {:ok, retrieved_list} = @subject.retrieve_uniq_in_range(conn, min_key, max_key)
        flush_storage(conn)
        assert Enum.all?(stored_binaries, fn stored_binary -> stored_binary in retrieved_list end)
      end
    end

    @tag :slow
    property "returned list never contains duplicate values", %{redis_conn: conn} do
      check all list_of_data    <- StreamData.list_of({storage_key_gen(), list_of_binary_gen()}, min_length: 1),
                keys_list       =  Enum.map(list_of_data, fn {key, _v} -> key end),
                max_key         =  Enum.max(keys_list),
                min_key         =  Enum.min(keys_list) do
        for {key, list} <- list_of_data do
          add_to_set(conn, key, list)
          add_to_sorted_set(conn, @storage_index_key, key, key)
        end
        {:ok, retrieved_list} = @subject.retrieve_uniq_in_range(conn, min_key, max_key)
        flush_storage(conn)

        uniqualized_retrieved_list = MapSet.new(retrieved_list) |> MapSet.to_list()
        assert Enum.count(retrieved_list) == Enum.count(uniqualized_retrieved_list)
      end
    end
  end
end
