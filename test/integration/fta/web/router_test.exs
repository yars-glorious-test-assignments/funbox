defmodule RouterTest do
  use ExUnitProperties
  use ExUnit.Case, async: false

  import JSONHelpers
  import HTTPRequestHelpers
  import MiscGenerators
  import WebHelpers

  @sbj_scheme "http"
  @sbj_host "localhost"
  @sbj_port "4001"
  @subject_url "#{@sbj_scheme}://#{@sbj_host}:#{@sbj_port}"

  @headers [{"content-type", "application/json"}]
  @request_headers @headers
  @expected_response_headers @headers

  describe "Hitting not existing route" do
    property "returns status 404 when we hit any adress except created routes" do
      get_routes = ["/visited_domains"]
      post_routes = ["/visited_links"]
      options_routes = get_routes ++ post_routes
      existing_routes = %{
        get: get_routes,
        post: post_routes,
        options: options_routes
      }

      check all(
        {verb, path} <- StreamData.filter(
          http_verb_path_pair_gen(), fn {verb, path} ->
            existing_paths = existing_routes[verb]
            not(
              is_list(existing_paths) and path in existing_paths
            )
            # and String.starts_with?(path, "/")
        end)
      ) do
        url = URI.parse(@subject_url) |> URI.merge(path) |> URI.to_string()
        {status_code, body, resp_headers} = http_req!(verb, url, @request_headers)

        assert ^status_code = 404
        regular_error_body = body == %{"status" => "error"} |> to_json
        head_body = body == "" and verb == :head
        assert regular_error_body or head_body
        assert_response_headers(resp_headers, @expected_response_headers)
      end
    end
  end
end
