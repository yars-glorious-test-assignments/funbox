defmodule VisitedDomainsEndpointTest do
  use ExUnitProperties
  use ExUnit.Case, async: false

  import HitsGenerators
  import HTTPRequestHelpers
  import JSONHelpers
  import MiscGenerators
  import RedisHelpers
  import WebHelpers

  @this_app :fta
  @redis_process_name_key :redis_process_name

  @headers [{"content-type", "application/json"}]
  @request_headers @headers
  @expected_response_headers @headers

  @sbj_scheme "http"
  @sbj_host "localhost"
  @sbj_port "4001"
  @sbj_path "/visited_domains"
  @subject_url "#{@sbj_scheme}://#{@sbj_host}:#{@sbj_port}#{@sbj_path}"

  @app FTA.App

  describe "GET #{@sbj_path}" do
    property "returns error when `from` is NOT provided" do
      check all(
        {from_timestamp, to_timestamp} <- non_desc_int_range_gen(),
        hits_strictly_before_from_ts <- hits_strictly_before_gen(from_timestamp),
        hits_exactly_at_from_ts <- hits_exactly_at_gen(from_timestamp),
        hits_between_timestamps  <- hits_between_gen(from_timestamp, to_timestamp),
        hits_exactly_at_to_ts <- hits_exactly_at_gen(to_timestamp),
        hits_strictly_after_to_ts <- hits_strictly_after_gen(to_timestamp),

        requested_hits = Enum.concat([
          hits_exactly_at_from_ts,
          hits_between_timestamps,
          hits_exactly_at_to_ts,
        ]),

        unaffected_hits = Enum.concat([
          hits_strictly_before_from_ts,
          hits_strictly_after_to_ts,
        ]),

        all_hits = requested_hits ++ unaffected_hits,

        {:ok, _rest} = flush_storage(Application.get_env(@this_app, @redis_process_name_key))
      ) do
        for hit <- all_hits do
          {ts, {link, _domain}} = hit
          @app.process_links(ts, [link])
        end

        query = URI.encode_query(%{"from" => from_timestamp})

        {code, body, resp_headers} =
          @subject_url
          |> URI.merge("#{@subject_url}?#{query}")
          |> URI.to_string()
          |> get!(@request_headers)

        parsed_body = from_json(body)

        assert ^code = 400
        assert_response_headers(resp_headers, @expected_response_headers)
        assert %{"status" => "error"} = parsed_body

        flush_storage(Application.get_env(@this_app, @redis_process_name_key))
      end
    end

    property "returns error when `to` is NOT provided" do
      check all(
        {from_timestamp, to_timestamp} <- non_desc_int_range_gen(),
        hits_strictly_before_from_ts <- hits_strictly_before_gen(from_timestamp),
        hits_exactly_at_from_ts <- hits_exactly_at_gen(from_timestamp),
        hits_between_timestamps  <- hits_between_gen(from_timestamp, to_timestamp),
        hits_exactly_at_to_ts <- hits_exactly_at_gen(to_timestamp),
        hits_strictly_after_to_ts <- hits_strictly_after_gen(to_timestamp),

        requested_hits = Enum.concat([
          hits_exactly_at_from_ts,
          hits_between_timestamps,
          hits_exactly_at_to_ts,
        ]),

        unaffected_hits = Enum.concat([
          hits_strictly_before_from_ts,
          hits_strictly_after_to_ts,
        ]),

        all_hits = requested_hits ++ unaffected_hits,

        {:ok, _rest} = flush_storage(Application.get_env(@this_app, @redis_process_name_key))
      ) do
        for hit <- all_hits do
          {ts, {link, _domain}} = hit
          @app.process_links(ts, [link])
        end

        query = URI.encode_query(%{"to" => to_timestamp})

        {code, body, resp_headers} =
          @subject_url
          |> URI.merge("#{@subject_url}?#{query}")
          |> URI.to_string()
          |> get!(@request_headers)

        parsed_body = from_json(body)

        assert ^code = 400
        assert_response_headers(resp_headers, @expected_response_headers)
        assert %{"status" => "error"} = parsed_body

        flush_storage(Application.get_env(@this_app, @redis_process_name_key))
      end
    end

    property "returns error when both parameters are omitted" do
      check all(
        {from_timestamp, to_timestamp} <- non_desc_int_range_gen(),
        hits_strictly_before_from_ts <- hits_strictly_before_gen(from_timestamp),
        hits_exactly_at_from_ts <- hits_exactly_at_gen(from_timestamp),
        hits_between_timestamps  <- hits_between_gen(from_timestamp, to_timestamp),
        hits_exactly_at_to_ts <- hits_exactly_at_gen(to_timestamp),
        hits_strictly_after_to_ts <- hits_strictly_after_gen(to_timestamp),

        requested_hits = Enum.concat([
          hits_exactly_at_from_ts,
          hits_between_timestamps,
          hits_exactly_at_to_ts,
        ]),

        unaffected_hits = Enum.concat([
          hits_strictly_before_from_ts,
          hits_strictly_after_to_ts,
        ]),

        all_hits = requested_hits ++ unaffected_hits,

        {:ok, _rest} = flush_storage(Application.get_env(@this_app, @redis_process_name_key))
      ) do
        for hit <- all_hits do
          {ts, {link, _domain}} = hit
          @app.process_links(ts, [link])
        end

        query = URI.encode_query(%{})

        {code, body, resp_headers} =
          @subject_url
          |> URI.merge("#{@subject_url}?#{query}")
          |> URI.to_string()
          |> get!(@request_headers)

        parsed_body = from_json(body)

        assert ^code = 400
        assert_response_headers(resp_headers, @expected_response_headers)
        assert %{"status" => "error"} = parsed_body

        flush_storage(Application.get_env(@this_app, @redis_process_name_key))
      end
    end

    property "returns error when params are not integer" do
      check all(
        {malformed_from_timestamp, malformed_to_timestamp} <- {non_integer_string_gen(), non_integer_string_gen()},
        {from_timestamp, to_timestamp} <- non_desc_int_range_gen(),
        hits_strictly_before_from_ts <- hits_strictly_before_gen(from_timestamp),
        hits_exactly_at_from_ts <- hits_exactly_at_gen(from_timestamp),
        hits_between_timestamps  <- hits_between_gen(from_timestamp, to_timestamp),
        hits_exactly_at_to_ts <- hits_exactly_at_gen(to_timestamp),
        hits_strictly_after_to_ts <- hits_strictly_after_gen(to_timestamp),

        requested_hits = Enum.concat([
          hits_exactly_at_from_ts,
          hits_between_timestamps,
          hits_exactly_at_to_ts,
        ]),

        unaffected_hits = Enum.concat([
          hits_strictly_before_from_ts,
          hits_strictly_after_to_ts,
        ]),

        all_hits = requested_hits ++ unaffected_hits,

        {:ok, _rest} = flush_storage(Application.get_env(@this_app, @redis_process_name_key))
      ) do
        for hit <- all_hits do
          {ts, {link, _domain}} = hit
          @app.process_links(ts, [link])
        end

        query = URI.encode_query(%{"from" => malformed_from_timestamp, "to" => malformed_to_timestamp})

        {code, body, resp_headers} =
          @subject_url
          |> URI.merge("#{@subject_url}?#{query}")
          |> URI.to_string()
          |> get!(@request_headers)

        parsed_body = from_json(body)

        assert code == 400
        assert_response_headers(resp_headers, @expected_response_headers)
        assert %{"status" => "error"} = parsed_body

        flush_storage(Application.get_env(@this_app, @redis_process_name_key))
      end
    end

    property "returns exactly [from; to] part of statistics when both to and from are provided and from <= to" do
      check all(
        {from_timestamp, to_timestamp} <- non_desc_int_range_gen(),
        hits_strictly_before_from_ts <- hits_strictly_before_gen(from_timestamp),
        hits_exactly_at_from_ts <- hits_exactly_at_gen(from_timestamp),
        hits_between_timestamps  <- hits_between_gen(from_timestamp, to_timestamp),
        hits_exactly_at_to_ts <- hits_exactly_at_gen(to_timestamp),
        hits_strictly_after_to_ts <- hits_strictly_after_gen(to_timestamp),

        requested_hits = Enum.concat([
          hits_exactly_at_from_ts,
          hits_between_timestamps,
          hits_exactly_at_to_ts,
        ]),

        unaffected_hits = Enum.concat([
          hits_strictly_before_from_ts,
          hits_strictly_after_to_ts,
        ]),

        all_hits = requested_hits ++ unaffected_hits,
        requested_hits_domains = Enum.map(requested_hits, fn {_ts, {_link, domain}} -> domain end),
        unique_requested_domains = MapSet.new(requested_hits_domains),
        visible_domains = MapSet.to_list(unique_requested_domains),

        {:ok, _rest} = flush_storage(Application.get_env(@this_app, @redis_process_name_key))
      ) do
        for hit <- all_hits do
          {ts, {link, _domain}} = hit
          @app.process_links(ts, [link])
        end

        query = URI.encode_query(%{"from" => from_timestamp, "to" => to_timestamp})

        {code, body, resp_headers} =
          @subject_url
          |> URI.merge("#{@subject_url}?#{query}")
          |> URI.to_string()
          |> get!(@request_headers)

        parsed_body = from_json(body)
        parsed_body_domains = parsed_body["domains"]

        assert ^code = 200
        assert_response_headers(resp_headers, @expected_response_headers)
        assert %{"status" => "ok"} = parsed_body
        assert visible_domains -- parsed_body_domains == []
        assert parsed_body_domains -- visible_domains == []

        flush_storage(Application.get_env(@this_app, @redis_process_name_key))
      end
    end

    property "returns error when from > to" do
      check all(
        {from_timestamp, to_timestamp} <- desc_int_range_gen(),
        hits_strictly_before_from_ts <- hits_strictly_before_gen(from_timestamp),
        hits_exactly_at_from_ts <- hits_exactly_at_gen(from_timestamp),
        hits_between_timestamps  <- hits_between_gen(from_timestamp, to_timestamp),
        hits_exactly_at_to_ts <- hits_exactly_at_gen(to_timestamp),
        hits_strictly_after_to_ts <- hits_strictly_after_gen(to_timestamp),

        requested_hits = Enum.concat([
          hits_exactly_at_from_ts,
          hits_between_timestamps,
          hits_exactly_at_to_ts,
        ]),

        unaffected_hits = Enum.concat([
          hits_strictly_before_from_ts,
          hits_strictly_after_to_ts,
        ]),

        all_hits = requested_hits ++ unaffected_hits,

        {:ok, _rest} = flush_storage(Application.get_env(@this_app, @redis_process_name_key))
      ) do
        for hit <- all_hits do
          {ts, {link, _domain}} = hit
          @app.process_links(ts, [link])
        end

        query = URI.encode_query(%{"from" => from_timestamp, "to" => to_timestamp})

        {code, body, resp_headers} =
          @subject_url
          |> URI.merge("#{@subject_url}?#{query}")
          |> URI.to_string()
          |> get!(@request_headers)

        parsed_body    = from_json(body)

        assert ^code = 400
        assert_response_headers(resp_headers, @expected_response_headers)
        assert %{"status" => "error"} = parsed_body

        flush_storage(Application.get_env(@this_app, @redis_process_name_key))
      end
    end

    property "retuns error when link to redis is broken" do
      invalid_conn_name = :invalid_conn
      {:ok, _pid} = Redix.start_link(host: "invalidhost", port: 1234, name: invalid_conn_name)
      old_redis_process = Application.get_env(@this_app, @redis_process_name_key)
      Application.put_env(@this_app, @redis_process_name_key, invalid_conn_name)

      check all(
        {from_timestamp, to_timestamp} <- non_desc_int_range_gen()
      )
      do
        query = URI.encode_query(%{"from" => from_timestamp, "to" => to_timestamp})

        {code, body, resp_headers} =
          @subject_url
          |> URI.merge("#{@subject_url}?#{query}")
          |> URI.to_string()
          |> get!(@request_headers)

        assert ^code = 500
        assert_response_headers(resp_headers, @expected_response_headers)
        assert body == %{status: :error} |> to_json
      end

      Application.put_env(@this_app, @redis_process_name_key, old_redis_process)
    end

    property "retuns error when link to redis is not started" do
      invalid_conn_name = :invalid_conn

      old_redis_process = Application.get_env(@this_app, @redis_process_name_key)
      Application.put_env(@this_app, @redis_process_name_key, invalid_conn_name)

      check all(
        {from_timestamp, to_timestamp} <- non_desc_int_range_gen()
      )
      do
        query = URI.encode_query(%{"from" => from_timestamp, "to" => to_timestamp})

        {code, body, resp_headers} =
          @subject_url
          |> URI.merge("#{@subject_url}?#{query}")
          |> URI.to_string()
          |> get!(@request_headers)

        assert ^code = 500
        assert_response_headers(resp_headers, @expected_response_headers)
        assert body == %{status: :error} |> to_json
      end

      Application.put_env(@this_app, @redis_process_name_key, old_redis_process)
    end
  end

  describe "OPTIONS #{@sbj_path}" do
    test "handles OPTIONS verb like no problem" do
      expected_headers = {"Allow", "OPTIONS, GET"}
      {code, _body, resp_headers} = options!(@subject_url, @request_headers)

      assert ^code = 204
      assert expected_headers in resp_headers
    end
  end
end
