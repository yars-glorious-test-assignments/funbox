defmodule VisitedLinksEndpointTest do
  use ExUnitProperties
  use ExUnit.Case, async: false

  import CustomMatchers
  import HTTPRequestHelpers
  import JSONHelpers
  import RedisHelpers
  import URIDataGenerators
  import WebHelpers

  @this_app :fta
  @redis_process_name_key :redis_process_name

  @storage_index_key "storage_index"

  @headers [{"content-type", "application/json"}]
  @request_headers @headers
  @expected_response_headers @headers

  @sbj_scheme "http"
  @sbj_host "localhost"
  @sbj_port "4001"
  @sbj_path "/visited_links"
  @subject_url "#{@sbj_scheme}://#{@sbj_host}:#{@sbj_port}#{@sbj_path}"

  @request_timestamp_delta_in_seconds 1

  describe "POST /visited_links endpoint" do
    test "returns error when they sent unparsable json" do
      req_body = """
        {unparsable json
      """

      {code, body, resp_headers} = post!(@subject_url, req_body, @request_headers)

      assert ^code = 400
      assert_response_headers(resp_headers, @expected_response_headers)
      assert body == to_json(%{status: :error})
    end

    test "returns error if body is parsable but not a list of binary" do
      invalid_input_variants = [\
        nil,
        "",
        "just string",
        %{"links" => [[], "string", %{a: :b}]},
        %{links: ["test1.ru", "funbox.ru", "http://ya.ru", ["asd,", "sda"]]}
      ]

      for input_variant <- invalid_input_variants do
        req_body = input_variant |> to_json()

        {code, body, resp_headers} = post!(@subject_url, req_body, @request_headers)

        assert ^code = 400
        assert_response_headers(resp_headers, @expected_response_headers)
        assert body == %{status: :error} |> to_json
      end
    end

    test "returns error when links list is empty" do
      req_body = %{links: []} |> to_json()
      {code, body, resp_headers} = post!(@subject_url, req_body, @request_headers)

      assert ^code = 400
      assert_response_headers(resp_headers, @expected_response_headers)
      assert body == %{status: :error} |> to_json
    end

    test "returns error when links list is nil" do
      req_body = %{links: nil} |> to_json()
      {code, body, resp_headers} = post!(@subject_url, req_body, @request_headers)

      assert ^code = 400
      assert_response_headers(resp_headers, @expected_response_headers)
      assert body == %{status: :error} |> to_json
    end

    test "returns error when links list is empty string" do
      req_body = %{links: ""} |> to_json()
      {code, body, resp_headers} = post!(@subject_url, req_body, @request_headers)

      assert ^code = 400
      assert_response_headers(resp_headers, @expected_response_headers)
      assert body == %{status: :error} |> to_json
    end

    test "retuns error when link to redis is broken" do
      invalid_conn_name = :invalid_conn

      {:ok, _pid} = Redix.start_link(host: "invalidhost", port: 1234, name: invalid_conn_name)
      old_redis_process = Application.get_env(@this_app, @redis_process_name_key)
      Application.put_env(@this_app, @redis_process_name_key, invalid_conn_name)

      [links] = Enum.take(list_of_links_gen(), 1)

      req_body = %{links: links} |> to_json()

      {code, body, resp_headers} = post!(@subject_url, req_body, @request_headers)
      Application.put_env(@this_app, @redis_process_name_key, old_redis_process)

      assert ^code = 500
      assert_response_headers(resp_headers, @expected_response_headers)
      assert body == %{status: :error} |> to_json
    end

    test "retuns error when link to redis is not started" do
      invalid_conn_name = :non_existent_pid

      old_redis_process = Application.get_env(@this_app, @redis_process_name_key)
      Application.put_env(@this_app, @redis_process_name_key, invalid_conn_name)

      [links] = Enum.take(list_of_links_gen(), 1)

      req_body = %{links: links} |> to_json()

      {code, body, resp_headers} = post!(@subject_url, req_body, @request_headers)
      Application.put_env(@this_app, @redis_process_name_key, old_redis_process)

      assert ^code = 500
      assert_response_headers(resp_headers, @expected_response_headers)
      assert body == %{status: :error} |> to_json
    end

    property "nothing is persisted when there is at least one incorrect link" do
      check all(
        links <- list_of_links_gen(),
        a_bad_link <- bad_link_gen()
      ) do
        shuffled_links = Enum.shuffle([a_bad_link | links])
        valid_body = %{links: shuffled_links} |> to_json

        time_now  = :erlang.system_time(:second)
        time_from = time_now - @request_timestamp_delta_in_seconds
        time_to   = time_now + @request_timestamp_delta_in_seconds

        {code, body, _resp_headers} = post!(@subject_url, valid_body, @request_headers)

        conn = Application.get_env(@this_app, @redis_process_name_key)

        # assert that given url is within the delta of timestamp
        assert {:ok, []} = get_sorted_set_members_by_score(conn, @storage_index_key, time_from, time_to)
        assert ^code = 400
        assert body == %{status: :error} |> to_json
      end
    end

    property "persist all domains from valid list with key==current timestamp" do
      check all(
        list_of_links_and_domain_names <- list_of_links_and_domain_names_gen(),
        {links, domain_names} = {
          Enum.map(list_of_links_and_domain_names, fn {l, _dn} -> l end),
          Enum.map(list_of_links_and_domain_names, fn {_l, dn} -> dn end)
        }
      ) do
        valid_body = %{links: links} |> to_json

        time_now  = :erlang.system_time(:second)
        time_from = time_now - @request_timestamp_delta_in_seconds
        time_to   = time_now + @request_timestamp_delta_in_seconds

        {code, body, _resp_headers} = post!(@subject_url, valid_body, @request_headers)

        conn = Application.get_env(@this_app, @redis_process_name_key)

        # assert that given url is within the delta of timestamp
        assert {:ok, [key]} = get_sorted_set_members_by_score(conn, @storage_index_key, time_from, time_to)

        assert ^code = 200
        assert body == %{status: :ok} |> to_json
        for domain_name <- domain_names do
          assert domain_name |> stored_in_redis_set?(at_key: key, at: conn)
        end
        flush_storage(conn)
      end
    end
  end

  describe "OPTIONS /visited_links endpoint" do
    test "handles OPTIONS verb like no problem" do
      expected_headers = {"Allow", "OPTIONS, POST"}
      {code, _body, resp_headers} = options!(@subject_url, @request_headers)

      assert ^code = 204
      assert expected_headers in resp_headers
    end
  end
end
