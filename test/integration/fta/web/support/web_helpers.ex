defmodule WebHelpers do
  @moduledoc """
    Helpers for writing web-related tests in integration environment.
  """
  import ExUnit.Assertions, only: [assert: 1]
  def assert_response_headers(returned_headers, expected_resp_headers) do
    for expected_header <- expected_resp_headers do
      assert expected_header in returned_headers
    end
  end
end
