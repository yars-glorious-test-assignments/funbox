defmodule JSONHelpers do
  @moduledoc """
    This module contains JSON helpers to be used in tests.
  """
  defdelegate to_json(data), to: Jason, as: :encode!
  defdelegate from_json(data), to: Jason, as: :decode!
end
