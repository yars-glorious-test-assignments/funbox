defmodule CustomMatchers.StoredInRedisSortedSet do
  @moduledoc """
    Module contaning a `stored_in_redis_sorted_set?` matcher
  """
  @spec stored_in_redis_sorted_set?(binary(), [at_key: binary(), at: binary()]) :: boolean()
  def stored_in_redis_sorted_set?(given_value, at_key: key, at: redis_conn) do
    case extract_values(redis_conn, key) do
      {:error, _reason}   -> raise "Redis link is down, check if redis instance is running and and the config is correct"
      {:ok, nil}          -> false
      {:ok, stored_values} -> compare_values(stored_values, given_value)
    end
  end

  @spec stored_in_redis_sorted_set?(binary(), [with_score: integer(), at_key: binary(), at: binary()]) :: boolean()
  def stored_in_redis_sorted_set?(given_value, with_score: score, at_key: key, at: redis_conn) do
    case extract_score(redis_conn, key, given_value) do
      {:error, _reason}    -> raise "Redis link is down, check if redis instance is running and and the config is correct"
      {:ok, nil}           -> false
      {:ok, indexed_score} -> "#{score}" == indexed_score
    end
  end

  def extract_values(conn, key) do
    Redix.command(conn, ["ZRANGE", key, 0, -1])
  end

  def extract_score(conn, key, value) do
    Redix.command(conn, ["ZSCORE", key, value])
  end

  defp compare_values(stored_values, given_value) do
    Enum.any?(stored_values, fn element -> element === given_value end)
  end
end

defmodule CustomMatchers.StoredInRedisSet do
  @moduledoc """
    Module contaning a `stored_in_redis_set?` matcher
  """
  @spec stored_in_redis_set?(binary(), [at_key: binary(), at: binary()]) :: boolean()
  def stored_in_redis_set?(given_value, at_key: key, at: redis_conn) do
    case extract_values(redis_conn, key) do
      {:error, _reason}   -> raise "Redis link is down, check if redis instance is running and and the config is correct"
      {:ok, nil}          -> false
      {:ok, stored_values} -> compare_values(stored_values, given_value)
    end
  end

  defp extract_values(conn, key) do
    Redix.command(conn, ["SMEMBERS", key])
  end

  defp compare_values(stored_values, given_value) do
    Enum.any?(stored_values, fn element -> element === given_value end)
  end
end

defmodule CustomMatchers do
  @moduledoc """
    A module to `import` to other files to bring all the custom matcher
  """
  defdelegate stored_in_redis_set?(given_value, opts), to: CustomMatchers.StoredInRedisSet
  defdelegate stored_in_redis_sorted_set?(given_value, opts), to: CustomMatchers.StoredInRedisSortedSet
end
