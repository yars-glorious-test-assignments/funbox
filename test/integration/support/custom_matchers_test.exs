defmodule CustomMatchersTest do
  use ExUnit.Case, async: false

  import RedisHelpers

  @subject CustomMatchers

  setup_all do
    redis_port = Application.get_env(:fta, :redis_port) |> String.to_integer()
    {:ok, conn} = Redix.start_link(host: Application.get_env(:fta, :redis_host), port: redis_port)
    {:ok, invalid_conn} = Redix.start_link(host: "invalidconn", port: 15_999)

    [redis_conn: conn, invalid_conn: invalid_conn]
  end

  setup %{redis_conn: redis_conn} do
    redis_conn
    |> flush_storage()

    []
  end

  describe "stored_in_redis_set? helper" do
    test "raises an when redis link are down", %{invalid_conn: invalid_conn} do
      assert_raise(RuntimeError, "Redis link is down, check if redis instance is running and and the config is correct", fn ->
        "any binary" |> @subject.stored_in_redis_set?(at_key: "any", at: invalid_conn)
      end)
    end

    test "returns false when the set is empty", %{redis_conn: conn} do
      refute "any binary" |> @subject.stored_in_redis_set?(at_key: "any", at: conn)
    end

    test "retuns false when there is no matching value inside the set", %{redis_conn: conn} do
      key = "any"
      add_to_set(conn, key, "other binary")
      refute "binary" |> @subject.stored_in_redis_set?(at_key: key, at: conn)
    end

    test "returns true when there is a matching value in the set", %{redis_conn: conn} do
      key = "any"
      Enum.map(["some binary", "binary", "another one"], &(add_to_set(conn, key, &1)))
      assert "binary" |> @subject.stored_in_redis_set?(at_key: key, at: conn)
    end
  end

  describe "stored_in_redis_sorted_set?(val, [at_key:, at:]) helper" do
    test "raises an when redis link are down", %{invalid_conn: invalid_conn} do
      assert_raise(RuntimeError, "Redis link is down, check if redis instance is running and and the config is correct", fn ->
        "any binary" |> @subject.stored_in_redis_sorted_set?(at_key: "any", at: invalid_conn)
      end)
    end

    test "returns false when the set is empty", %{redis_conn: conn} do
      refute "any binary" |> @subject.stored_in_redis_sorted_set?(at_key: "any", at: conn)
    end

    test "retuns false when there is no matching value inside the set", %{redis_conn: conn} do
      key = "any"
      add_to_sorted_set(conn, key, "other binary")
      refute "binary" |> @subject.stored_in_redis_sorted_set?(at_key: key, at: conn)
    end

    test "returns true when there is a matching value in the set", %{redis_conn: conn} do
      key = "any"
      Enum.map(["some binary", "binary", "another one"], &(add_to_sorted_set(conn, key, &1)))
      assert "binary" |> @subject.stored_in_redis_sorted_set?(at_key: key, at: conn)
    end
  end

  describe "stored_in_redis_sorted_set?(val, [with_score:, at_key:, at:]) helper" do
    test "raises an when redis link are down", %{invalid_conn: invalid_conn} do
      assert_raise(RuntimeError, "Redis link is down, check if redis instance is running and and the config is correct", fn ->
        "any binary" |> @subject.stored_in_redis_sorted_set?(with_score: 0, at_key: "any", at: invalid_conn)
      end)
    end

    test "returns false when the set is empty", %{redis_conn: conn} do
      score = 1
      refute "any binary" |> @subject.stored_in_redis_sorted_set?(with_score: score, at_key: "any", at: conn)
    end

    test "retuns false when there is no matching value inside the set", %{redis_conn: conn} do
      key = "any"
      score = 1
      add_to_sorted_set(conn, key, "binary", score)
      refute "other binary" |> @subject.stored_in_redis_sorted_set?(with_score: score, at_key: key, at: conn)
    end

    test "retuns false when there is matching value inside the set has different score", %{redis_conn: conn} do
      key = "any"
      score = 1
      add_to_sorted_set(conn, key, "binary", score + 10)
      refute "binary" |> @subject.stored_in_redis_sorted_set?(with_score: score, at_key: key, at: conn)
    end

    test "returns true when there is a matching value in the set with given score", %{redis_conn: conn} do
      key = "any"
      score = 10
      Enum.map(["some binary", "binary", "another one"], &(add_to_sorted_set(conn, key, &1, score)))
      assert "binary" |> @subject.stored_in_redis_sorted_set?(with_score: score, at_key: key, at: conn)
    end
  end
end
