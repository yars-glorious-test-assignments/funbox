defmodule HTTPRequestHelpers do
  @moduledoc """
    Module defines HTTP request wrappers to create unified abstraction for http requests to be used in tests
  """

  def get!(url, headers) do
    %{
      status_code: status_code,
      body: body,
      headers: headers
    } = HTTPoison.get!(url, headers)

    {status_code, body, headers}
  end

  def post!(url, body, headers) do
    %{
      status_code: status_code,
      body: body,
      headers: headers
    } = HTTPoison.post!(url, body, headers)

    {status_code, body, headers}
  end

  def options!(url, headers) do
    %{
      status_code: status_code,
      body: body,
      headers: headers
    } = HTTPoison.options!(url, headers)

    {status_code, body, headers}
  end

  def http_req!(verb, url, body \\ "", headers) do
    %{
      status_code: status_code,
      body: body,
      headers: headers
    } = HTTPoison.request!(verb, url, body, headers)

    {status_code, body, headers}
  end
end
