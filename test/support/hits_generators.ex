defmodule HitsGenerators do
  @moduledoc """
   The module contains high-level generators to produce link visits with timestamp with given requirements.
  """
  import URIDataGenerators

  @offlimit_data_gen_size_fixed_adjustment 10
  @offlimit_data_gen_size_scaling_factor 2

  @spec hit_strictly_before_gen(integer()) :: {StreamData.integer(), StreamData.t({String.t(), String})}
  def hit_strictly_before_gen(timestamp) do
    ts  =
      StreamData.integer()
      |> StreamData.scale(fn size ->
        @offlimit_data_gen_size_fixed_adjustment + @offlimit_data_gen_size_scaling_factor * size
      end)
      |> StreamData.filter(& &1 < timestamp)
    hit_data = link_and_domain_name_gen()

    {ts, hit_data}
  end

  def hit_strictly_after_gen(timestamp) do
    ts  =
      StreamData.integer()
      |> StreamData.scale(fn size ->
        @offlimit_data_gen_size_fixed_adjustment + @offlimit_data_gen_size_scaling_factor * size
      end)
      |> StreamData.filter(& &1 > timestamp)
    hit_data = link_and_domain_name_gen()
    {ts, hit_data}
  end

  def hit_exactly_at_gen(timestamp) do
    ts  = StreamData.integer(timestamp..timestamp)
    hit_data = link_and_domain_name_gen()
    {ts, hit_data}
  end

  def hit_between_gen(from_timestamp, to_timestamp) do
    ts  = StreamData.integer(from_timestamp..to_timestamp)
    hit_data = link_and_domain_name_gen()
    {ts, hit_data}
  end

  def hits_strictly_before_gen(timestamp),  do: StreamData.list_of(hit_strictly_before_gen(timestamp),  min_length: 0)
  def hits_strictly_after_gen(timestamp),   do: StreamData.list_of(hit_strictly_after_gen(timestamp),   min_length: 0)
  def hits_exactly_at_gen(timestamp),       do: StreamData.list_of(hit_exactly_at_gen(timestamp),       min_length: 0)
  def hits_between_gen(from_ts, to_ts),     do: StreamData.list_of(hit_between_gen(from_ts, to_ts),     min_length: 0)
end
