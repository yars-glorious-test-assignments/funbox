defmodule RedisHelpers do
  @moduledoc """
    A module containing helper functions for redis interactions
  """

  def flush_storage(redis_conn) do
    {:ok, _} = Redix.command(redis_conn, ["FLUSHDB"])
  end

  def add_to_set(redis_conn, key, values) when is_list(values) do
    Redix.command(redis_conn, ["SADD", key] ++ values)
  end

  def add_to_set(redis_conn, key, value) do
    Redix.command(redis_conn, ["SADD", key, value])
  end

  def add_to_sorted_set(redis_conn, key, value, score \\ 0) do
    Redix.command(redis_conn, ["ZADD", key, score, value])
  end

  def get_sorted_set_members(redis_conn, key) do
    Redix.command(redis_conn, ["ZRANGE", key, 0, -1])
  end

  def get_sorted_set_members_by_score(redis_conn, key, from, to) do
    Redix.command(redis_conn, ["ZRANGEBYSCORE", key, from, to])
  end

  def get_set_members(redis_conn, key) do
    Redix.command(redis_conn, ["SMEMBERS", key])
  end
end
