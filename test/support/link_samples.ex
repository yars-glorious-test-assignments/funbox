defmodule LinkSamples do
  @moduledoc """
    A module contaning test collection to run against various similar tests which have to do with domain_name validation
  """

  def good_ones do
    ~w(
      test.ru
      local
      local/
      localhost
      funbox.ru
      https://test.com/path/?q='123'&page=20
      funbox.ru/#main
      https://10.20.30.50.test.com/path/?q='123'&page=20
      ya.ru
      https%3A%2F%2Felixir-lang.org
      %D0%B0%D0%BD%D0%B5%D0%BA%D0%B4%D0%BE%D1%82.%D1%80%D1%84
      xn--80ahdnilort5k.xn--p1ai
      анекдот.рф
      user@test.com
    )
  end

  def bad_ones do
    strange_ones() ++ ip_address_as_host_ones() ++ bad_formated_ones()
  end

  def strange_ones() do
    [nil, "", "  ", "https://  ", "\n"]
  end

  def ip_address_as_host_ones() do
    ~w(
      [2001:0db8:85a3:0000:0000:8a2e:0370:7334]/somepath
      https://10.20.32.16/path/?q='123'&page=20
      https://10.20.32.16.29/path/?q='123'&page=20
    )
  end

  def bad_formated_ones do
    ~w(
      test..com
      test.........com
      http://localhost|
      http://|
      |
      http://local|host
      http://|localhost
      http://&localhost
      &localhost
      local&host
      localhost&
      //?a=b
      http://
      http:///a
      http:///a/
    )
  end
end
