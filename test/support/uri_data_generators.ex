defmodule URIDataGenerators do
  @moduledoc """
    This module contains handy generators and formatters for URL parser (property-based) testing
  """

  use ExUnitProperties

  @max_subdomain_length 63
  @max_domain_pieces 10
  @max_path_part_length 256
  @min_query_key_length 1
  @max_query_key_length 20

  @spec bad_link_gen :: StreamData.t(binary())
  def bad_link_gen() do
    StreamData.one_of(
      Enum.map(LinkSamples.bad_ones(), &StreamData.constant(&1))
    )
  end

  @spec list_of_bad_links_gen :: StreamData.t(list)
  def list_of_bad_links_gen() do
    StreamData.list_of(bad_link_gen(), min_length: 1)
  end

  @spec scheme_gen :: StreamData.t(binary())
  def scheme_gen() do
    ["", "http://", "https://"]
    |> Enum.map(&StreamData.constant(&1))
    |> StreamData.one_of()
  end

  @spec domain_piece_gen :: StreamData.t(list)
  def domain_piece_gen() do
    [
      {10, StreamData.string(:alphanumeric, length: 1)},
      {1, StreamData.constant("-")}
    ]
    |> StreamData.frequency()
    |> StreamData.filter(& String.match?(&1, ~r/[[:alpha:]]/))
    |> StreamData.list_of(min_length: 1, max_length: @max_subdomain_length)
    |> StreamData.list_of(min_length: 1, max_length: @max_domain_pieces)
  end

  @spec domain_name_fmt(list(list(binary()))) :: binary
  def domain_name_fmt(domain_piece) do
    domain_piece
    |> Enum.map(fn list -> List.to_string(list) end)
    |> Enum.join(".")
  end

  @spec path_list_gen :: StreamData.t(list(String.t()))
  def path_list_gen() do
    StreamData.string(:alphanumeric, min_length: 1, max_length: @max_path_part_length)
    |> StreamData.list_of()
  end

  @spec path_fmt(list(binary())) :: binary()
  def path_fmt(paths) do
    paths
    |> Enum.map(&"/#{&1}")
    |> Enum.join("")
  end

  @spec path_ending_gen :: StreamData.t(binary())
  def path_ending_gen() do
    ["", "/"]
    |> Enum.map(&StreamData.constant(&1))
    |> StreamData.one_of()
  end

  @spec query_pairs_gen :: %StreamData{generator: (any, any -> map)}
  def query_pairs_gen() do
    {
      StreamData.string(:alphanumeric,
        min_length: @min_query_key_length,
        max_length: @max_query_key_length
      ),
      StreamData.string(:alphanumeric)
    }
    |> StreamData.list_of()
  end

  @spec query_string_fmt(list({binary(), binary()})) :: binary
  def query_string_fmt(query_pairs) do
    query_pairs
    |> Enum.map(fn {k, v} -> "#{k}=#{v}" end)
    |> Enum.join("&")
    |> (fn
          text when text === "" -> ""
          text -> "?#{text}"
        end).()
  end

  @spec link_gen :: StreamData.t(binary())
  def link_gen() do
    gen all(
      scheme <- scheme_gen(),
      domain_piece <- domain_piece_gen(),
      domain_name = domain_name_fmt(domain_piece),
      path_list <- path_list_gen(),
      path = path_fmt(path_list),
      path_ending <- path_ending_gen(),
      query_pairs <- query_pairs_gen(),
      query = query_string_fmt(query_pairs)
    ) do
      "#{scheme}#{domain_name}#{path}#{path_ending}#{query}"
    end
  end

  @spec list_of_links_gen :: StreamData.t(list(binary()))
  def list_of_links_gen() do
    StreamData.list_of(link_gen(), min_length: 1)
  end

  @spec link_and_domain_name_gen :: StreamData.t({String.t(), String.t()})
  def link_and_domain_name_gen() do
    StreamData.bind(link_gen(), fn link ->
      uri = normalized_uri(link)
      downcased_host = downcase_nilable(Map.get(uri, :host))
      {
        StreamData.constant(link),
        StreamData.constant(downcased_host)
      }
    end)
  end

  defp normalized_uri(link) do
    uri = URI.parse(link)
    case Map.get(uri, :host) do
      nil -> URI.parse("//#{link}")
      _   -> uri
    end
  end

  defp downcase_nilable(nil), do: nil
  defp downcase_nilable(string) do
    String.downcase(string)
  end

  def list_of_links_and_domain_names_gen() do
    StreamData.list_of(link_and_domain_name_gen(), min_length: 1)
  end

  @spec path_gen :: StreamData.t(binary())
  def path_gen() do
    gen all(
      path_list <- path_list_gen(),
      path = path_fmt(path_list),
      path_ending <- path_ending_gen(),
      query_pairs <- query_pairs_gen(),
      query = query_string_fmt(query_pairs)
    ) do
      path = "#{path}#{path_ending}#{query}"
      if String.starts_with?(path, "?"), do: "/#{path}", else: path
    end
  end
end
