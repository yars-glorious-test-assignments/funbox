defmodule MiscGenerators do
  @moduledoc """
    A module containing generator helpers for property-based testing
  """
  import URIDataGenerators

  @spec storage_key_gen :: StreamData.t(integer())
  def storage_key_gen(), do: StreamData.integer()

  @spec list_of_binary_gen :: StreamData.t(list(binary()))
  def list_of_binary_gen() do
    StreamData.list_of(StreamData.binary(), min_length: 1)
  end

  @spec int_range_gen() :: {StreamData.t(integer()), StreamData.t(integer())}
  def int_range_gen(), do: {StreamData.integer(), StreamData.integer()}

  @spec non_desc_int_range_gen :: StreamData.t({integer(), integer()})
  def non_desc_int_range_gen() do
    StreamData.bind(
      int_range_gen(),
      fn
        {a, b} when a > b -> StreamData.constant({b, a})
        {a, b}            -> StreamData.constant({a, b})
      end
    )
  end

  @spec desc_int_range_gen :: StreamData.t({integer(), integer()})
  def desc_int_range_gen() do
    int_range_gen()
    |> StreamData.bind(
      fn
        {a, b} when a > b -> StreamData.constant({a, b})
        {a, b}            -> StreamData.constant({b, a})
      end
    )
    |> StreamData.filter(fn ({a, b}) -> a != b end)
  end

  @spec non_integer_string_gen :: StreamData.t(String.t())
  def non_integer_string_gen() do
    StreamData.string(:alphanumeric)
    |> StreamData.filter(fn str ->
      not(
        with {_int, rem} <- Integer.parse(str),
             has_parsed  <- rem == ""
        do
          has_parsed
        else
          _errors -> false
        end
      )
    end)
  end

  def http_verb_gen() do
    StreamData.one_of([
      :get,
      :head,
      :options,
      :put,
      :post,
      :delete,
      :patch,
      :link,
      :unlink
    ])
  end

  def http_verb_path_pair_gen(), do: {
    http_verb_gen(),
    path_gen()
  }
end
